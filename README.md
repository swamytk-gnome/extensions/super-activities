<h1>Super Activities</h1>

It is a GNOME Shell Extension to be displayed on top panel. This extension replaces the default plain Actitivies button on top bar. If this extension is disabled, it can bring back the default one. This extension is informative on panel and functional too. It displays two buttons.<br><br>1. One button is to to display distribution logo and version. Clicking this button shows App menu.<br>
2.Second button is to display GNOME logo and version. Clicking this button shows Workspace menu (tasks).<br>


<h2>Ubuntu</h2>
<img src="screenshots/ubuntu.png"
     alt="Super Activities on Ubuntu"
     style="margin-right: 10px;" />

<h2>Pop!-OS</h2>
<img src="screenshots/popos.png"
     alt="Super Activities on Pop!-OS"
     style="margin-right: 10px;" /><br>

<h2>Fedora</h2>
<img src="screenshots/fedora.png"
     alt="Super Activities on Fedora"
     style="margin-right: 10px;" /><br>

<h2>Manjaro</h2>
<img src="screenshots/manjaro.png"
     alt="Super Activities on Manjaro"
     style="margin-right: 10px;" /><br>

