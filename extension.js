/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* Import all necessary modules */
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib
const Gio = imports.gi.Gio;
const Main = imports.ui.main;
const Config = imports.misc.config;

/* /etc/os-release file, which is mandatory for this extension to work */
const distro_info_file	= "/etc/os-release";
const default_distro_name = "GNU/Linux";
const icons_dir_name = "icons";
const default_distro_icon_path = "icons/default_distro.png"
const gnome_icon_path = "icons/gnome.png"
const space_between_buttons = "  ";

let default_activities = Main.panel.statusArea['activities'];
let boxExtension;
let panelButtonDistro;
let panelButtonGnome;

// The below distro IDs are supported with right logo on button,
//   otherwise default one shown
var distro_ids = [ "debian", "ubuntu", "pop", "arch", "fedora", "manjaro", 
	"opensuse-leap", "opensuse-tumbleweed", "opensuse", "linuxmint", "centos", 
	"elementary", "gentoo", "nixos", "rhel", "slackware", "sles", 
	"solus", "void" ];

class Extension {
    constructor() {
    }

    enable() {
    		default_activities.container.hide();
		Main.panel._leftBox.insert_child_at_index(boxExtension, -1);
    }

    disable() {
    		default_activities.container.show();    
		Main.panel._leftBox.remove_child(boxExtension);
    }
}

function init() {

	var index;
	var token;
	var value;
	let distroName = "";
	let distroVersion = "";
	let distroLogo = "";

	/* Collect Distro data from /etc/os-release file */
	let os_release = Shell.get_file_contents_utf8_sync(distro_info_file).toString();
	var os_entries = os_release.split("\n");
	for (index = 0; index < os_entries.length; index++) {
		token = os_entries[index].split("=")[0]; 
		value = os_entries[index].split("=")[1]; 
		if (token == "NAME") {
			distroName = value; 
		} else if (token == "VERSION_ID") {
			distroVersion = value; 
		} else if (token == "ID") {
			distroId = value; 
		} 
	}

	/* Handling exception cases of above data */
	// If no distro name token found, assign default one
	if (distroName == "") {
		distroName = default_distro_name;
	}
	// If any space in distro name, leave out remaining part
	distroName = distroName.split(" ")[0];	
	// If any double quote in distro name, remove it - do twice for both the sides
	distroName = distroName.replace('"', ''); 
	distroName = distroName.replace('"', ''); 
	// If any double quote in distro version id, remove it - do twice for both the sides
	distroVersion = distroVersion.replace('"', '');
	distroVersion = distroVersion.replace('"', '');

	/* Collect GNOME data from Config module imported here */
	let gnomeVersion = Config.PACKAGE_VERSION;

	/* Creating Icons */
	if ( distro_ids.indexOf(distroId) >= 0 ) {
		this.distroIcon = Gio.icon_new_for_string(`${Me.path}/${icons_dir_name}/${distroId}.png`);
	} else { // icon not supported, so using default one for rest of distros
		this.distroIcon = Gio.icon_new_for_string(`${Me.path}/${default_distro_icon_path}`);
	}
	let panelIconDistro = new St.Icon({
		gicon: this.distroIcon,
		style_class: 'activity-icon systeminfo-icon-style'
	});
	this.gnomeIcon = Gio.icon_new_for_string(`${Me.path}/${gnome_icon_path}`);
	let panelIconGnome = new St.Icon({
		gicon: this.gnomeIcon,
		style_class: 'activity-icon systeminfo-icon-style'
	});

	/* Creating labels */
	let panelButtonDistroText = new St.Label({
    	text : distroName + " " + distroVersion + space_between_buttons,
	    y_align : Clutter.ActorAlign.CENTER
    });
	let panelButtonGnomeText = new St.Label({
    	text : gnomeVersion + space_between_buttons,
        y_align : Clutter.ActorAlign.CENTER
    });

	/* Creating buttons */
	panelButtonDistro = new St.Button({
            style_class : "panel-button",
        });
	panelButtonGnome = new St.Button({
            style_class : "panel-button",
        });

	/* Creating Boxes to accomodate Icon and label */
	boxExtension = new St.BoxLayout();
	boxDistro = new St.BoxLayout();
	boxGnome = new St.BoxLayout();

	/* Add box to Button and then add Icon & Text to Box */
	panelButtonDistro.add_actor(boxDistro);
	boxDistro.add_actor(panelIconDistro);
	boxDistro.add_actor(panelButtonDistroText);
	panelButtonGnome.add_actor(boxGnome);
	boxGnome.add_actor(panelIconGnome);
	boxGnome.add_actor(panelButtonGnomeText);

	/* Add all Buttons to Extension level top box */
	boxExtension.add_actor(panelButtonDistro);
	boxExtension.add_actor(panelButtonGnome);

	/* Attaching event handlers to buttons */
	panelButtonDistro.connect('clicked', () => this.apps_overview());
	panelButtonGnome.connect('clicked', () => this.ws_overview());

    return new Extension();
}

function apps_overview()
{
	// If already showing apps overview, hide it
	if (Main.overview.visible && Main.overview.viewSelector._showAppsButton.checked) {
		Main.overview.hide();
		return;
	}
	// Here apps overview is not shown, so configure for apps overview
	Main.overview.viewSelector._showAppsButton.checked = true;
	if (!Main.overview.visible)
		Main.overview.show();	// No overview shown, hence show apps overview
	else // already showing workspace overview, hence toggle to app overview
        Main.overview.viewSelector._showAppsButtonToggled(); 
}

function ws_overview()
{
	// If already showing workspace overview, hide it
	if (Main.overview.visible && !Main.overview.viewSelector._showAppsButton.checked) {
			Main.overview.hide();
			return;
	}
	// Here workspace overview is not shown, so configure for workspace overview
	Main.overview.viewSelector._showAppsButton.checked = false;
	if (!Main.overview.visible)
		Main.overview.show(); // No overview shown, hence show workspace overview
	else // already showing apps overview, hence toggle to workspace overview
        Main.overview.viewSelector._showAppsButtonToggled();
}

